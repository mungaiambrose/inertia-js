const mix = require('laravel-mix')
const path = require('path')

mix
  .setPublicPath('src/main/resources/static/')
  .js('src/main/assets/js/main.js', 'js')
  .postCss('src/main/assets/css/main.css', 'css', [require('tailwindcss'),])
  .webpackConfig({
    output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.runtime.esm.js',
        '@': path.resolve('src/main/assets/js'),
      },
    },
  })
  .sourceMaps()
