package com.walulife.inertiajs.todo;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.NonNull;

public class Repository {

  private AtomicInteger index;
  private Set<Todo> todos;

  public Repository() {
    this.index = new AtomicInteger(0);
    this.todos = Stream.of("Buy food", "Masturbate", "Code App")
        .map(title -> createTodo(title))
        .collect(Collectors.toSet());
  }

  public Set<Todo> allTodos() {
    return todos;
  }

  public Todo addTodo(@NonNull String title) {
    Todo todo = createTodo(title);
    todos.add(todo);
    return todo;
  }

  public Optional<Todo> deleteTodo(int id) {
    Optional<Todo> optionalTodo = todos.stream().filter(todo1 -> todo1.getId()==id).findFirst();
    optionalTodo.ifPresent(todo -> todos.remove(todo));
    return optionalTodo;
  }

  public Optional<Todo> changeDoneStatus(int id, boolean status) {
    Optional<Todo> optionalTodo = todos.stream().filter(todo1 -> todo1.getId()==id).findFirst();
    optionalTodo.ifPresent(todo -> todo.setFinished(status));
    System.out.println("Changed status of todo:" + id + " to:" + status );
    return optionalTodo;
  }

  private Todo createTodo(String title) {
    Todo todo = new Todo();
    todo.setId(index.incrementAndGet());
    todo.setTitle(title);
    todo.setFinished(true);
    todo.setCreatedAt(LocalDateTime.now(Clock.systemUTC()));
    return todo;
  }


}
