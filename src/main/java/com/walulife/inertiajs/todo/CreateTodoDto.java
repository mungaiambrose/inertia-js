package com.walulife.inertiajs.todo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CreateTodoDto {
  @NotNull
  @NotBlank
  private String title;
}
