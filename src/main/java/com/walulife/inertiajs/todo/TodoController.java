package com.walulife.inertiajs.todo;

import com.walulife.inertiajs.inertia.Response;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TodoController {

  private Repository repo;
  private TodoModelAssembler todoModelAssembler;

  public TodoController(TodoModelAssembler assembler) {
    this.repo = new Repository();
    this.todoModelAssembler = assembler;
  }

  @GetMapping
  public Response allTodos(HttpServletRequest request) {
    Response response = new Response(request, "Todo/Index");
    //repo.allTodos().stream().map(todo -> new EntityModel<>(todo))
    response.setData(todoModelAssembler.toCollectionModel(repo.allTodos()));
    return response;
  }

  @PostMapping
  public Response createTodo(HttpServletRequest request, @Valid @RequestBody CreateTodoDto dto, BindingResult bindingResult) {
    Response response = new Response(request, "Todos");
    if (!bindingResult.hasErrors()) {
      Todo createdTodo = repo.addTodo(dto.getTitle());
      response.addSuccessMessage(createdTodo.getTitle() + " created");
    }
    response.addErrors(bindingResult);
    response.setData(repo.allTodos());
    return response;
  }

  @PatchMapping("/{id}")
  public ResponseEntity<?> markDone(@PathVariable Integer id, @RequestBody ChangeDoneStatus status) {
    Optional<Todo> todo = repo.changeDoneStatus(id, status.isStatus());
    return todo.map(data -> ResponseEntity.ok().build()).orElse(ResponseEntity.notFound().build());
  }

  @DeleteMapping("/{id}")
  public Response delete(HttpServletRequest request, @PathVariable Integer id) {
    Response response = new Response(request, "Todos");
    Optional<Todo> deletedTodo = repo.deleteTodo(id);
    deletedTodo.ifPresent(todo -> response.addSuccessMessage(todo.getTitle() + " deleted"));
    response.setData(repo.allTodos());
    return response;
  }
}
