package com.walulife.inertiajs.todo;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Component
public class TodoModelAssembler implements SimpleRepresentationModelAssembler<Todo> {

  @Override
  public void addLinks(EntityModel<Todo> resource) {
    Todo todo = resource.getContent();
    resource.add(linkTo(methodOn(TodoController.class).markDone(todo.getId(), null)).withSelfRel());
  }

  @Override
  public void addLinks(CollectionModel<EntityModel<Todo>> resources) {
    resources.add(linkTo(methodOn(TodoController.class).createTodo(null, new CreateTodoDto(), null)).withRel("add"));
  }
}
