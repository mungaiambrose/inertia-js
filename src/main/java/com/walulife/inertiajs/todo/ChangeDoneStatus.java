package com.walulife.inertiajs.todo;

import lombok.Data;
import lombok.NonNull;

@Data
public class ChangeDoneStatus {
  private boolean status;
}
