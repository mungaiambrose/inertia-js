package com.walulife.inertiajs.todo;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"id"})
public class Todo {
  private int id;
  private String title;
  private boolean finished;
  private LocalDateTime createdAt;
}
