package com.walulife.inertiajs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InertiajsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InertiajsApplication.class, args);
	}

}
