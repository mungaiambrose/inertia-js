package com.walulife.inertiajs;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.walulife.inertiajs.inertia.Response;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Configuration
@RestController
public class WelcomeController {

  @PostMapping("/age")
  public Response tryFailure(
        HttpServletRequest request,
        @Valid @RequestBody TestForm form, BindingResult result) {
    Response response = new Response(request, "Test");
    System.out.println(form);
    if (result.hasErrors()) {
      response.addErrors(result);
    }
    TestFormAssembler assembler = new TestFormAssembler();
    response.setData(assembler.toModel(form));
    return response;
  }

  static  class TestFormAssembler implements SimpleRepresentationModelAssembler<TestForm> {

    @Override
    public void addLinks(EntityModel<TestForm> resource) {
      Link link = linkTo(methodOn(WelcomeController.class).tryFailure(null, null, null)).withRel("showAge");
      resource.add(link);
    }

    @Override
    public void addLinks(CollectionModel<EntityModel<TestForm>> resources) {
      Link link = linkTo(methodOn(WelcomeController.class).tryFailure(null, null, null)).withRel("showAge");
      resources.add(link);
    }
  }
}
