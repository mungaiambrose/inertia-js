package com.walulife.inertiajs.security;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import lombok.Data;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;

@Data
public class LoginPage {
  private boolean loggedOut = false;
  private boolean  invalidInput = false;

  static class LoginPageAssembler implements SimpleRepresentationModelAssembler<LoginPage> {

    @Override
    public void addLinks(EntityModel<LoginPage> resource) {
      Link loginPage = linkTo(SecurityController.class).slash("/login").withRel("login");
      Link logoutPage = linkTo(SecurityController.class).slash("/logout").withRel("logout");
      resource.add(loginPage, logoutPage);
    }

    @Override
    public void addLinks(CollectionModel<EntityModel<LoginPage>> resources) {

    }
  }
}
