package com.walulife.inertiajs.security;

import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NonNull;

@Data
public class ChangeUsernameDto {
  @NonNull
  @NotBlank
  private String username;
}
