package com.walulife.inertiajs.security;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import lombok.NonNull;
import lombok.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.SimpleRepresentationModelAssembler;

@Value(staticConstructor = "from")
public class UsernameDto {
  @NonNull
  private String username;

  public EntityModel<UsernameDto> toModel() {
    UsernameDtoAssembler assembler = new UsernameDtoAssembler();
    return assembler.toModel(this);
  }

  static class UsernameDtoAssembler implements SimpleRepresentationModelAssembler<UsernameDto> {

    @Override
    public void addLinks(EntityModel<UsernameDto> resource) {
     Link selfLink = linkTo(methodOn(SecurityController.class).viewUsername(null, null)).withSelfRel();
     resource.add(selfLink);
    }

    @Override
    public void addLinks(CollectionModel<EntityModel<UsernameDto>> resources) {

    }
  }
}
