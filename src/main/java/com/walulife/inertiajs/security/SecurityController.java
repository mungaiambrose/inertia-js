package com.walulife.inertiajs.security;

import com.walulife.inertiajs.inertia.Response;
import java.security.Principal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
public class SecurityController {

  @GetMapping("/login")
  public Response loginPage(HttpServletRequest request) {
    Response response = new Response(request, "Security/Login");
    LoginPage data = new LoginPage();

    Map<String, String[]> paramMap = request.getParameterMap();
    if (paramMap.containsKey("error")) {
      data.setInvalidInput(true);
    }
    if (paramMap.containsKey("logout")) {
      data.setLoggedOut(true);
    }

    LoginPage.LoginPageAssembler assembler = new LoginPage.LoginPageAssembler();
    response.setData(assembler.toModel(data));
    return response;
  }


  @GetMapping("/username")
  public Response viewUsername(HttpServletRequest request, Principal principal) {
    Response response = new Response(request, "Security/Username");
    UsernameDto dto = UsernameDto.from(principal.getName());
    response.setData(dto.toModel());
    return response;
  }

  @PostMapping("/username")
  public Response changeUsername(HttpServletRequest request, @Valid @RequestBody ChangeUsernameDto dto, BindingResult result) {
    Response response = new Response(request, "Security/Username");
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (result.hasErrors()) {
      response.addErrors(result);
      return viewUsername(request, (Principal) auth.getPrincipal());
    }
    return null;
  }
}
