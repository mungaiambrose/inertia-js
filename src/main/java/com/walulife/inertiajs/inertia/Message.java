package com.walulife.inertiajs.inertia;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(of = {"message"})
class Message {

  private Type type;
  private String message;

  public static Message error(@NonNull String error) {
    return new Message(Type.ERROR, error);
  }

  public static Message info(@NonNull String message) {
    return new Message(Type.INFO, message);
  }

  public static Message warning(@NonNull String warning) {
    return new Message(Type.WARNING, warning);
  }

  public static Message success(@NonNull String message) {
    return new Message(Type.SUCCESS, message);
  }

  static enum Type {
    WARNING, ERROR, INFO, SUCCESS;
  }
}
