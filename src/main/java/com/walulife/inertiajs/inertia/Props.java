package com.walulife.inertiajs.inertia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor
@Getter
@EqualsAndHashCode
class Props {
  private Set<Message> messages = new HashSet<>();
  private Map<String, List<String>> errors = Collections.emptyMap();
  private Object data;

  public void addMessage(@NonNull Message message) {
    this.messages.add(message);
  }

  public void addError(@NonNull String field, @NonNull String message) {
    if (!this.errors.containsKey(field)) {
      List<String> listMessages = new ArrayList<>();
      this.errors.put(field, listMessages);
    }
    this.errors.get(field).add(message);
  }

  public void addData(@NonNull Object objectData) {
    this.data = objectData;
  }
}
