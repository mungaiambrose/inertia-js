package com.walulife.inertiajs.inertia;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@RestControllerAdvice
public class InertiaResponseModifier implements ResponseBodyAdvice<Response> {
  public static final String X_INERTIA = "x-inertia";
  public static final String X_INERTIA_CONVERT_HTML = "x-inertia-html";
  BuildProperties properties;

  public InertiaResponseModifier(BuildProperties properties) {
    this.properties = properties;
  }

  @Override
  public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
    return Response.class.equals(returnType.getMethod().getReturnType());
  }

  @Override
  public Response beforeBodyWrite(
        Response body,
        MethodParameter returnType,
        MediaType selectedContentType,
        Class<? extends HttpMessageConverter<?>> selectedConverterType,
        ServerHttpRequest request,
        ServerHttpResponse response) {
    body.setVersion(getVersion());
    if (request.getHeaders().containsKey(X_INERTIA)) {
      response.getHeaders().add(X_INERTIA, "true");
      response.getHeaders().add("Vary", "Accept");
    } else {
      response.getHeaders().add("Content-Type", "text/html");
      response.getHeaders().add(X_INERTIA_CONVERT_HTML, "true");
    }
    return body;
  }

  private String getVersion() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssn").withZone(ZoneId.of("UTC"));
    return formatter.format(properties.getTime());
  }
}
