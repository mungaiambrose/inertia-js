package com.walulife.inertiajs.inertia;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component
public class InertiaResponseConvertToHtmlFilter extends OncePerRequestFilter {

  private SpringTemplateEngine templateEngine;

  public InertiaResponseConvertToHtmlFilter(SpringTemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    ByteArrayResponseWrapper wrapper = new ByteArrayResponseWrapper(response);
    filterChain.doFilter(request, wrapper);

    byte[] serResponse = wrapper.toByteArray();

    if (isAnInertiaResponseToConvertToHtml(response)) {
      String template = getTemplate(serResponse);
      response.setContentLength(template.length());
      response.setContentType("text/html");
      response.getOutputStream().write(template.getBytes());
    } else {
      response.getOutputStream().write(serResponse);
    }
  }

  private boolean isAnInertiaResponseToConvertToHtml(HttpServletResponse response) {
    return response.containsHeader(InertiaResponseModifier.X_INERTIA_CONVERT_HTML);
  }

  private String getTemplate(byte[] response) {
    Context context = new Context();
    context.setVariable("page", new String(response));
    return templateEngine.process("inertiajs", context);
  }

  static class ByteArrayServletOutputStream extends ServletOutputStream {

    ByteArrayOutputStream baos;

    public ByteArrayServletOutputStream(ByteArrayOutputStream baos) {
      this.baos = baos;
    }

    @Override
    public boolean isReady() {
      return true;
    }

    @Override
    public void setWriteListener(WriteListener listener) {
    }

    @Override
    public void write(int i) throws IOException {
      baos.write(i);
    }
  }

  static class ByteArrayResponseWrapper extends HttpServletResponseWrapper {

    private boolean usingWriter;
    private ByteArrayOutputStream output;

    public ByteArrayResponseWrapper(HttpServletResponse response) {
      super(response);
      this.output = new ByteArrayOutputStream();
      this.usingWriter = false;
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
      if (usingWriter) {
        return super.getOutputStream();
      }
      usingWriter = true;
      return new ByteArrayServletOutputStream(output);
    }

    @Override
    public PrintWriter getWriter() throws IOException {
      if (usingWriter) {
        return super.getWriter();
      }
      usingWriter = true;
      return new PrintWriter(output);
    }

    public byte[] toByteArray() {
      return output.toByteArray();
    }
  }
}
