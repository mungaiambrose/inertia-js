package com.walulife.inertiajs.inertia;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@EqualsAndHashCode
@Getter
public class Response {

  private String component;
  private String url;
  private Props props = new Props();
  @Setter
  private String version = "";

  public Response(@NonNull HttpServletRequest request, @NonNull String component) {
    this.url = fullUrl(request);
    this.component = component;
  }

  public void addErrors(@NonNull BindingResult bindingResult) {
    List<FieldError> fieldErrors = bindingResult.getFieldErrors();
    fieldErrors.forEach(fieldError -> props.addError(fieldError.getField(), fieldError.getDefaultMessage()));
  }

  public void setData(@NonNull Object data) {
    props.addData(data);
  }

  private String fullUrl(HttpServletRequest request) {
    StringBuilder requestUrl = new StringBuilder(request.getRequestURL().toString());
    String queryString = request.getQueryString();

    if (queryString == null) {
      return requestUrl.toString();
    }
    return requestUrl.append('?').append(queryString).toString();
  }

  public void addSuccessMessage(@NonNull String message) {
    props.addMessage(Message.success(message));
  }

  public void addInformationMessage(@NonNull String message) {
    props.addMessage(Message.info(message));
  }

  public void addWarningMessage(@NonNull String message) {
    props.addMessage(Message.warning(message));
  }

  public void addErrorMessage(@NonNull String message) {
    props.addMessage(Message.error(message));
  }

}
