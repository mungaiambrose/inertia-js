package com.walulife.inertiajs;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TestForm {

  @NotNull
  @Max(200)
  private Integer age;
}
