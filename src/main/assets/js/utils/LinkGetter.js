export const linkFromResource = (resource, rel) => resource.links.filter(link => link.rel === rel)[0].href;
