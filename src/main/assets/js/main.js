import Vue from 'vue'
import Inertia from 'inertia-vue'
import { plugin } from 'vue-function-api'
import Axios from 'axios';

Vue.use(Inertia)
Vue.use(plugin)

Axios.defaults.withCredentials=true;

const app = document.getElementById('app')

new Vue({
  render: h => h(Inertia, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => import(/* webpackChunkName: "[request]" */`@/Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(app)
